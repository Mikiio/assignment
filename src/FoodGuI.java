import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGuI {
    private JPanel root;
    private JLabel toplabel;
    private JButton PeperoncinoButton;
    private JButton CarbonaraButton;
    private JButton GenoveseButton;
    private JButton BologneseButton;
    private JButton TomatosupaButton;
    private JButton MentaisupaButton;
    private JButton checkOutButton;
    private JTextPane OrderedItem;
    private JLabel label;
    private JTextPane Totalcosts;

    int total;

    void order(String food, int cost) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + " ?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation == 0) {
            int confirmation2 = JOptionPane.showConfirmDialog(null,
                    "Would you use 10% off coupon?",
                    "Coupon Confirmation",
                    JOptionPane.YES_NO_OPTION);

            if (confirmation2 == 0) {
                JOptionPane.showMessageDialog(null,
                        "Thank you for your ordering " + food + "! It will be served as soon as possible.");
                String currentText = OrderedItem.getText();
                OrderedItem.setText(currentText + food + (cost * 0.9) + " yen" + "\n");

                total += (int) (cost * 0.9);
                Totalcosts.setText("Total  " + total + " yen");
            }
                if (confirmation2 == 1) {
                    JOptionPane.showMessageDialog(null,
                            "It Thank you for your ordering " + food + "! It will be served as soon as possible.");
                    String currentText = OrderedItem.getText();
                    OrderedItem.setText(currentText + food + cost + "yen " + "\n");
                    total += cost;
                    Totalcosts.setText("Total" + total + " yen");
                }
            }
        }


    public FoodGuI() {
        PeperoncinoButton.setIcon(new ImageIcon(
                this.getClass().getResource("./photo/Peperoncino.png")
        ));
        CarbonaraButton.setIcon(new ImageIcon(
                this.getClass().getResource("./photo/Carbonara.png")
        ));
        GenoveseButton.setIcon(new ImageIcon(
                this.getClass().getResource("./photo/Genovese.png")
        ));
        BologneseButton.setIcon(new ImageIcon(
                this.getClass().getResource("./photo/Bolognese.png")
        ));
        TomatosupaButton.setIcon(new ImageIcon(
                this.getClass().getResource("./photo/Tomatosupa.png")
        ));
        MentaisupaButton.setIcon(new ImageIcon(
                this.getClass().getResource("./photo/Mentaisupa.png")
        ));

        PeperoncinoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("Peperoncino",600);
            }
        });
        CarbonaraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                order("Carbonara",800);
            }
        });
        GenoveseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Genovese",650);
            }
            });

        BologneseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Meatsupa",750);
            }
        });
        TomatosupaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tomatosupa",700);
            }
        });
        MentaisupaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Mentaisupa",850);
            }
        });

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?" ,
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if(confirmation == 0){
                    JOptionPane.showMessageDialog(null,
                            " Thank you." +"The total price is " + total + " yen" );

                    OrderedItem.setText(null);
                    Totalcosts.setText(null);
                    total = 0;
                    Totalcosts.setText("Total" + total + "yen");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGuI");
        frame.setContentPane(new FoodGuI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
